use ftdi_embedded_hal::{self as hal, ftdi};
use ftx_lib::Ftx;
use std::cell::RefCell;

fn main() {
    let device = ftdi::find_by_vid_pid(0x0403, 0x6010)
        .interface(ftdi::Interface::B)
        .open()
        .unwrap();
    let hal = hal::FtHal::init_freq(device, 7_000).unwrap(); // 1 kHz
    let bus = RefCell::new(hal.i2c().unwrap());
    let mut ftx = Ftx::new(&bus);

    ftx.init_adc().unwrap();

    // Enable the LNA
    ftx.set_lna_power(false).unwrap();

    // Set the laser current to maximum
    ftx.set_ld_current(255).unwrap();

    println!("Temp: {:.2} C", ftx.get_temp().unwrap());
    println!("  PD: {:.2} uA", ftx.get_pd_current().unwrap());
    println!("  LD: {:.2} mA", ftx.get_ld_current().unwrap());
    println!(" LNA: {:.2} mA", ftx.get_lna_current().unwrap())
}
