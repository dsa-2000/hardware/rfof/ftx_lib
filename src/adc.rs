use crate::{Ftx, FtxResult};
use embedded_hal::i2c::I2c;
use tla2528::{Channel, Osr, PinMode};

const VREF: f32 = 5.0;

const TEMP: Channel = Channel::Ch0;
const PDI: Channel = Channel::Ch1;
const RF: Channel = Channel::Ch2;
const LNAI: Channel = Channel::Ch3;
const LDI: Channel = Channel::Ch4;
const LNA_FAULT: Channel = Channel::Ch5;
const LNA_EN: Channel = Channel::Ch6;

const PDI_SHUNT: f32 = 62.0;
const LDI_SHUNT: f32 = 1.0;
const LNAI_SHUNT: f32 = 0.5;

/// Current-sense amplifier gian
const GAIN: f32 = 100.0;

impl<'a, I2C, E> Ftx<'a, I2C>
where
    I2C: I2c<Error = E>,
{
    pub fn init_adc(&mut self) -> FtxResult<(), E> {
        // Calibrate and configure pins
        self.adc.calibrate()?;
        self.adc.set_pin_mode(TEMP, PinMode::Analog)?;
        self.adc.set_pin_mode(PDI, PinMode::Analog)?;
        self.adc.set_pin_mode(RF, PinMode::Analog)?;
        self.adc.set_pin_mode(LNAI, PinMode::Analog)?;
        self.adc.set_pin_mode(LDI, PinMode::Analog)?;
        self.adc.set_pin_mode(LNA_FAULT, PinMode::DigitalInput)?;
        self.adc.set_pin_mode(LNA_EN, PinMode::PushPullOutput)?;
        // Reset brown-out bit
        self.adc.bor_reset()?;
        // Enable oversampling
        self.adc.set_oversampling(Osr::_128)?;
        Ok(())
    }

    fn read_float(&mut self, chan: Channel) -> FtxResult<f32, E> {
        Ok(self.adc.analog_read(chan)? as f32
            / if self.adc.get_oversampling() == Osr::_0 {
                4095.0
            } else {
                65535.0
            })
    }

    fn read_current(&mut self, chan: Channel, shunt: f32) -> FtxResult<f32, E> {
        let raw = self.read_float(chan)?;
        Ok((raw * VREF) / (GAIN * shunt))
    }

    /// Get the temperature in celsius
    pub fn get_temp(&mut self) -> FtxResult<f32, E> {
        const SLOPE: f32 = 19.53; // mv/C
        const OFFSET: f32 = 400.0; // mV @ 0C
        let m_v = self.read_float(TEMP)? * VREF * 1000.0;
        Ok((m_v - OFFSET) / SLOPE)
    }

    /// Get the monitor photodiode current in uA
    pub fn get_pd_current(&mut self) -> FtxResult<f32, E> {
        Ok(self.read_current(PDI, PDI_SHUNT)? * 1e6)
    }

    /// Get the actual, sensed laser current in mA
    pub fn get_ld_current(&mut self) -> FtxResult<f32, E> {
        Ok(self.read_current(LDI, LDI_SHUNT)? * 1000.0)
    }

    /// Get the LNA current in mA
    pub fn get_lna_current(&mut self) -> FtxResult<f32, E> {
        Ok(self.read_current(LNAI, LNAI_SHUNT)? * 1000.0)
    }

    /// Enable/Disable the LNA
    pub fn set_lna_power(&mut self, enabled: bool) -> FtxResult<(), E> {
        Ok(self.adc.digital_write(LNA_EN, enabled)?)
    }

    /// Read the LNA overcurrent fault bit. Returns `true` on fault.
    pub fn get_lna_fault(&mut self) -> FtxResult<bool, E> {
        let fault = self.adc.digital_read(LNA_FAULT)?;
        Ok(!fault)
    }
}
