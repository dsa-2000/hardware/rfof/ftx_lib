#![no_std]

use core::cell::RefCell;
use digipot::Cat5171;
use embedded_hal::i2c::I2c;
use embedded_hal_bus::i2c::RefCellDevice;
use tla2528::{typestate::Initialized, Tla2528};

mod adc;
mod digipot;

const DIGIPOT_ADDR: u8 = 0x2C;
const ATT_ADDR: u8 = 0x20;
const ADC_ADDR: u8 = 0x10;
const UUID_ADDR: u8 = 0x10;
const EEPROM_ADDR: u8 = 0x50;

pub struct Ftx<'a, I2C> {
    // atten: TCA6408 bus expander to attenuator
    digipot: Cat5171<RefCellDevice<'a, I2C>>,
    //eeprom: AT24CSW010,
    /// TLA2528 ADC
    adc: Tla2528<RefCellDevice<'a, I2C>, Initialized>,
}

#[derive(Debug)]
/// FTX Error types
pub enum Error<E> {
    /// Lower-level ADC error
    Adc(tla2528::Error<E>),
    /// Lower-level digipot error
    Digipot(digipot::Error<E>),
}

impl<E> core::convert::From<tla2528::Error<E>> for Error<E> {
    fn from(e: tla2528::Error<E>) -> Self {
        Error::Adc(e)
    }
}

impl<E> core::convert::From<digipot::Error<E>> for Error<E> {
    fn from(e: digipot::Error<E>) -> Self {
        Error::Digipot(e)
    }
}

/// Result type for FTX commands
pub type FtxResult<T, E> = Result<T, Error<E>>;

impl<'a, I2C, E> Ftx<'a, I2C>
where
    I2C: I2c<Error = E>,
    E: core::fmt::Debug,
{
    /// Construct a new FTX instance.
    ///
    /// Note: The RefCell must be constructed in the higher-scope as we
    /// can't have self-referential structs.
    pub fn new(bus: &'a RefCell<I2C>) -> Self {
        // Create the shared bus for the children i2c devices
        let adc_bus = RefCellDevice::new(bus);
        let digipot_bus = RefCellDevice::new(bus);

        let adc = Tla2528::new(adc_bus, tla2528::Address::H10)
            .reset()
            .unwrap();
        //let eeprom = Eeprom24x::new_24csx01(&bus, eeprom24x::SlaveAddr::Default);
        let digipot = Cat5171::new(digipot_bus, false);
        Self { adc, digipot }
    }
}
