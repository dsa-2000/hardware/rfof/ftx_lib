use crate::{Ftx, FtxResult};
use embedded_hal::i2c::I2c;

const ADDR_BASE: u8 = 0b0101100;

/// AD5245-compatible 256-position digital potentiometer
pub struct Cat5171<I2C> {
    addr: u8,
    bus: I2C,
}

/// Digipot-specific errors
#[derive(Debug)]
pub enum Error<E> {
    I2c(E),
}

impl<E> core::convert::From<E> for Error<E> {
    fn from(e: E) -> Self {
        Error::I2c(e)
    }
}

impl<I2C, E> Cat5171<I2C>
where
    I2C: I2c<Error = E>,
{
    pub fn new(bus: I2C, ad0: bool) -> Self {
        Self {
            bus,
            addr: ADDR_BASE | ad0 as u8,
        }
    }

    pub fn set_state(&mut self, word: u8) -> Result<(), Error<E>> {
        // We don't care about reset to midscale or shutdown, so hard-code
        // the "instruction byte" to 0
        Ok(self.bus.write(self.addr, &[0, word])?)
    }

    pub fn get_state(&mut self) -> Result<u8, Error<E>> {
        let mut byte = [0u8; 1];
        self.bus.read(self.addr, &mut byte)?;
        Ok(byte[0])
    }
}

impl<'a, I2C, E> Ftx<'a, I2C>
where
    I2C: I2c<Error = E>,
{
    /// Set the laser current source to `word`.
    /// This word is 0-50mA mapping to 0-255.
    pub fn set_ld_current(&mut self, word: u8) -> FtxResult<(), E> {
        Ok(self.digipot.set_state(word)?)
    }

    /// Gets the state of the adjustable current soruce
    pub fn get_ld_current_setpoint(&mut self) -> FtxResult<u8, E> {
        Ok(self.digipot.get_state()?)
    }
}
